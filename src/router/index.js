import Vue from 'vue'
import Router from 'vue-router'
import LandingPage from '@/pages/landing-page'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landingPage',
      component: LandingPage
    }
  ]
})
