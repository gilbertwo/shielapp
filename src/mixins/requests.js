import moment from 'moment'
import axios from 'axios'

// const base_url = 'http://52.233.176.178:4000/'
const url_map = {
  'alpha': 'https://price.msstores.detailonline.com/api/views/backdoor',
  'live': 'https://live.detailonline.com/api/views/backdoor',
  'staging': 'https://microsoft.detailonline.com/api/views/backdoor'
}
const auth_map = {
  'FZNfMN_0Eee8oQANOijygQ': {
    company_name: 'Alpha Test',
    auth: 'eyJhbGciOiJIUzI1NiIsImV4cCI6MTU0NzkwMzMyMCwiaWF0IjoxNTQyNzE5MzIwfQ.eyJpZCI6InJNeFZodUNwRWVlOG9nQU5PaWp5Z1EifQ.-t6tdbWHnJEWg4XvM1qgXPzmUQwPKDUPBh4QlaZLiXA'
  },
  'G3TKVHDBEeamBZk63gYHyw': {
    company_name: 'Microsoft',
    auth: 'eyJhbGciOiJIUzI1NiIsImV4cCI6MTU0NzkwMzMwMywiaWF0IjoxNTQyNzE5MzAzfQ.eyJpZCI6IlBodENGRUx4RWVpOHd3QU5PaWp5Z1EifQ.CezQKKTsmaELYMAomFdEQRGCiE7teT0w0436lNV6wVQ'
  },
  'DFNLTng2Eei8ygANOijygQ': {
    company_name: 'MS Analytics',
    auth: 'eyJhbGciOiJIUzI1NiIsImV4cCI6MTU0NzkwMzI0NiwiaWF0IjoxNTQyNzE5MjQ2fQ.eyJpZCI6ImowVmdmSGhWRWVpOHlnQU5PaWp5Z1EifQ.CGaaVxAd05_XqPGMJJsJodnLs6DLTI4d60Dc6OaXuFo'
  }
}

export default {

  methods: {
    async getBrands(environment) {
      let options = {
        url: `${ url_map[environment] }/sheilapp/brands`,
        contentType: 'application/json',
        method: 'GET'
      }
      let result = null

      try {
        result = await axios(options)
      } catch (err) {
        // no action
      }

      return await result.data
    },

    async getCompanies(environment) {
      let options = {
        url: `${ url_map[environment] }/sheilapp/companies`,
        contentType: 'application/json',
        method: 'GET'
      }
      let result = null

      try {
        result = await axios(options)
      } catch (err) {
        // no action
      }

      return await result.data
    },

    async getRetailers(environment) {
      let options = {
        url: `${ url_map[environment] }/sheilapp/retailers`,
        contentType: 'application/json',
        method: 'GET'
      }
      let result = null

      try {
        result = await axios(options)
      } catch (err) {
        // no action
      }

      return await result.data
    },

    async getProductTypes(environment) {
      let options = {
        url: `${ url_map[environment] }/sheilapp/producttypes`,
        contentType: 'application/json',
        method: 'GET'
      }
      let result = null

      try {
        result = await axios(options)
      } catch (err) {
        // no action
      }

      return await result.data
    },

    getDump(data, environment) {
      let options = {
        url: `${ url_map[environment] }/sheilapp/dump`,
        contentType: 'application/json',
        method: 'POST',
        data: data
      }

      return axios(options)
    },

    async getProductsToHeaven() {
      let options = {
        url: `${ url_map['alpha'] }/upload/products`,
        contentType: 'application/json',
        method: 'GET'
      }
      let result = null

      try {
        result = await axios(options)

      } catch (err) {
        // no action
      }

      return await result.data
    },

    async saveProduct(comp_uuid, doc) {
      let options = {
        url: `${  url_map['alpha'] }/upload/product-item`,
        contentType: 'application/json',
        method: 'POST',
        data: {
          'default_company_uuid': comp_uuid,
          'defult_company_couch': this.generateCompanyDBName(comp_uuid),
          'token': auth_map[comp_uuid].auth,
          'item': doc
        }
      }
      let result = null

      try {
        result = await axios(options)

      } catch (err) {
        // no action
      }

      return await result.data
    }
  }
}
