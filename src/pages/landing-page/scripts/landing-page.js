import XLSX from 'xlsx'
import requests from '@/mixins/requests'

export default {
  name: 'landingPage',

  data() {
    return {
      excelLoading: false,
      companies: null,
      brands: null,
      retailers: null,
      productTypes: null,
      environments: [
        {label: 'Alpha', uuid: 'alpha'},
        {label: 'Live', uuid: 'live'},
        {label: 'Staging', uuid: 'staging'}
      ],
      status: [
        {label: 'All', uuid: 'all'},
        {label: 'Active', uuid: 'active'},
        {label: 'Not Active', uuid: 'not_active'}
      ],

      selEnvironment: 'alpha',
      selCompany: null,
      selBrand: null,
      selRetailer: null,
      selType: null,
      selStatus: null,
      shielaInit: false,

      // --- shila2 app data ---
      productsToUpload: null,
      uploadProgress: 0,
      uploadLoading: false,
      lowieCompanies: [],
      lowieEnvironments: [],

      selLowieEnvironment: 'alpha',
      selLowieCompany: null,
      lowieTable: []
    }
  },

  mixins: [
    requests
  ],

  created() {
    setTimeout(() => {
      this.shielaInit = true
    }, 4000)
  },

  mounted() {
    this.initData()
  },

  watch: {
    selEnvironment(uuid) {
      this.companies = null
      this.brands = null
      this.retailers = null
      this.productTypes = null

      if (uuid) {
        this.initData()
      }
    }
  },

  computed: {
  },

  methods: {
    async initData() {
      this.companies = await this.getCompanies(this.selEnvironment)
      if (this.companies && this.companies.length) {
        this.companies.unshift({
          company_name: 'All',
          company_uuid: 'all'
        })
      }

      this.brands = await this.getBrands(this.selEnvironment)
      if (this.brands && this.brands.length) {
        this.brands.unshift({
          brand_name: 'All',
          brand_uuid: 'all'
        })
      }

      this.retailers = await this.getRetailers(this.selEnvironment)
      if (this.retailers && this.retailers.length) {
        this.retailers.unshift({
          website_url: 'All',
          website_uuid: 'all'
        })
      }

      this.productTypes = await this.getProductTypes(this.selEnvironment)
      if (this.productTypes && this.productTypes.length) {
        this.productTypes.unshift({
          producttype_name: 'All',
          producttype_uuid: 'all'
        })
      }

      this.lowieCompanies = await this.getCompanies(this.selLowieEnvironment)
      // if (this.lowieCompanys && this.lowieCompanys.length) {
      //   this.lowieCompanys.unshift({
      //     company_name: 'All',
      //     company_uuid: 'all'
      //   })
      // }

    },

    generateCompanyDBName(compId) {
      let result = 'company-'
      let regex = /([A-Z]|[a-z0-9]{0,})([a-z0-9]{0,})/g

      result += compId.match(regex).map(val => {
      	let isUpperCase = (text) => {
      		return val.substr(0, 1) === val.substr(0, 1).toUpperCase()
      	}
      	return `${ (isUpperCase(val) && val.length? '_': '') + val.toLowerCase() }`
      }).join('')

      return result
    },

    async onUpload() {
      if (this.selLowieCompany) {
        let resp = await this.getProductsToHeaven()
        let totalProds = 0
        let saveCount = 0

        this.uploadProgress = 2
        this.lowieTable = []

        if (resp && resp.products) {
          totalProds = resp.products.length
          for (let i = 0; i < resp.products.length; i++) {
            try {
              let prodSaveResp = await this.saveProduct(this.selLowieCompany, resp.products[i])
              saveCount++
              this.uploadProgress = Math.floor((saveCount / totalProds) * 1e2)
              this.$notify({
                title: 'Excel',
                message: prodSaveResp.message,
                type: 'warning'
              });

              this.lowieTable.push(prodSaveResp)
              // console.log('prodSaveResp: ', prodSaveResp)
            } catch (err) {
              this.$notify({
                title: 'Excel',
                message: 'Failed to download excelsheet',
                type: 'warning'
              });
            }
          }

          setTimeout(() => {
            this.uploadProgress = 0
          }, 1000)
        }
        // console.log('products: ', products)
      }
      // this.uploadLoading = true
      // try {
      //   this.productsToUpload = await this.getProductsToHeaven()
      // } catch (err) {
      //   // no action
      // }
      // this.uploadLoading = false
      // console.log('productsToUpload:', this.productsToUpload)
    },

    convertToUploadable(prods) {
      let result = []

      if (prods && prods.length > 2) {
        for (let i = 2; i < prods.length; i++) {
          let prodModel = {
            brand: prods[i][0],
            category: prods[i][1],
            type: prods[i][2],
            name: prods[i][3],
            sku: prods[i][4],
            currency: prods[i][5],
            erp: prods[i][6],
            retailers: []
          }

          // if (prods[i].length > 7) {
          //   for (let j = 7; j < prods[i].length; (j + 2)) {
          //     try {
          //       prodModel.retailers.push({
          //         website:  prods[i][j],
          //         label:  prods[i][j + 1]
          //       })
          //     } catch (err) {
          //       // no action
          //     }
          //   }
          // }

          result.push(prodModel)
        }
      }

      return result
    },

    generateExcel(data) {
      let excelName = 'maam-Shie-important-reports.xlsx'
      let excelFile = XLSX.utils.book_new()
      let sheet = XLSX.utils.aoa_to_sheet(data)

      XLSX.utils.book_append_sheet(excelFile, sheet, 'Magandang buhay')
      XLSX.writeFile(excelFile, excelName)
      // console.log('data: ', data)
    },

    onGetDump() {
      let data = {
        company: this.selCompany? this.selCompany: 'all',
        brand: this.selBrand? this.selBrand: 'all',
        retailer: this.selRetailer? this.selRetailer: 'all',
        producttype: this.selType? this.selType: 'all',
        status: this.selStatus? this.selStatus: 'all'
      }

      this.excelLoading = true
      this.getDump(data, this.selEnvironment)
        .then(resp => {
          this.generateExcel(resp.data)
          this.excelLoading = false
          this.$notify({
            title: 'Excel',
            message: 'You have suuceesfully dowloaded the excel',
            type: 'success'
          });
        })
        .catch(err => {
          console.log('ERROR: ', err)
          this.excelLoading = false
          this.$notify({
            title: 'Excel',
            message: 'Failed to download excelsheet',
            type: 'warning'
          });
        })
    }
  }
}
